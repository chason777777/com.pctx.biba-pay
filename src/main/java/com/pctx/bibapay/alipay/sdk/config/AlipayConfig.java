package com.pctx.bibapay.alipay.sdk.config;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 */
public class AlipayConfig {

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2018030702333372";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCXou+UmZIXyqx3iMFRXSDEUxSKh8TOalvSWE7z13sdii8+Va0BKcBfORspW5oVmQc1nwPis+WHr36h8yz8CIniz2ArLKtTPEdg3Z0essbBAd+DuxTT61+azLxC7sVHlmMTiUm9jg7V65ASzbVTBSfAWiIyL55o/9EOagwT4Dh8ysVWUHw1bgdl4wv3PmpFSlg8+impJ5ALq/7u98JHaD2y8nvQIojfrGe1UjxI43ElUUrEa35UvEH16XHM0qGbF2KbAirNKm08B73bDthydJALJG85vE+KkETSgEW/q/0ub7v0MvEvye4CA1RmmLs9OiN3IQp3gWDYGsik/ZIkAoJXAgMBAAECggEAJ7vtVZ4MNynHmab/KSfjDy7KAibjfkdGek7jmWfMfggYqxfeNqDPJOSc5nB3ZtNrnTRm2z/SlIR7BUrobb57CzDV/Vo+Jph0B8DqjAHCrQ6lmCosof9aVQDzEALrNQAGRDdEWGRiJNIONa8rpW6DZRb5QPdTZiYqjMFjGs41XxWGHiPupI7FjIljUD4YdEhvjDPYM7cO1CBbBnq/Di8h8L2/0s1QaDC+DWVB3ipmsms4rDx0PZwle8wg4OMJhkX2kEFcU/xr4hCvrn6dHxFy3qpQQ9AzKbHbp+/ezQumZBapivXgQzA7N1DcH4Ms0FXeQj0K+YowTsgpRCHbxAhJyQKBgQDtrS2mCPMtxTmweUeulFUGtJnKI9xV6JC1/06bdAqSIkxy6A5uhhyzcavuPruxnLgm+Lz220RHzOK+z4XT8A6a8PkzVXTC7GDIqV0L7P49dMoQPhG6cARraxrYhkLoAA/wTXTj/itwCs81ZCAfXsTy3pEY2zbQXJa4YDYyPi8oqwKBgQCjU6hsLUOHN2c/HUj3IpyfpsQnxkzIxAa1+CV0ROkf23you4TjGLaQO0/awbAPmjg6Lecjq5xGToIqL8MKYVODA35pQ28gcV0U9WA7nSvCE3NqgR90YlazOtS6JXFwaqhmEJ8WDDW0MIkGiqDabTK9bAPrA0wK3tHz2fAHIKIlBQKBgGhRs5CFqx6yyzBe+wxwRKWNzVaAyOvYCPQo2/q1QxKDBWUF35e5O42zXdBTLpS1gcpxMJd1BBIzpm4zrbypR+/PxsE2R+pqT8yjMZjTa3/T7ACXDbb0BayUNRNGUsV7iYNiq+SKIcyqoeoASW0bbnxfHfZ3+SYZDXpdmEwTKSXDAoGAPKIwIojjSu2MjOfIUMcusi5tH/8EPnxXqXKIJRP4H/WR3+pNuSlRjVYXnTTOsdwlB6RG7P0mtQssU5ELrWzYXgkMAvQf60Fqtv8pTLhaqGAkNuPc0442da9fjgOci4ltHwcIqFy8CzsBg8YvVSe48I2LHtJIhriV5RuBeoPo7ikCgYAuXxstBLfyfVwjcPhqnq1iCiqLe2E+C2kiVf0/bRTvOx749akYQXxA2kIMLBL76nuL9lj1Nf5pN4FxDBUZv8mwGuEzvBvUDeHO+hELxVhm0Q1h3cik6rzfOgKQCKXo+OCm8kov0tv3G9F34FnVTb+vQPaeNyjgE2npBqrR0Ai3gg==";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm
    // 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl6LvlJmSF8qsd4jBUV0gxFMUiofEzmpb0lhO89d7HYovPlWtASnAXzkbKVuaFZkHNZ8D4rPlh69+ofMs/AiJ4s9gKyyrUzxHYN2dHrLGwQHfg7sU0+tfmsy8Qu7FR5ZjE4lJvY4O1euQEs21UwUnwFoiMi+eaP/RDmoME+A4fMrFVlB8NW4HZeML9z5qRUpYPPopqSeQC6v+7vfCR2g9svJ70CKI36xntVI8SONxJVFKxGt+VLxB9elxzNKhmxdimwIqzSptPAe92w7YcnSQCyRvObxPipBE0oBFv6v9Lm+79DLxL8nuAgNUZpi7PTojdyEKd4Fg2BrIpP2SJAKCVwIDAQAB";

    // 服务器异步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://localhost:8103/alipay/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://f.pctx.cn";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi.alipay.com/gateway.do";

}
