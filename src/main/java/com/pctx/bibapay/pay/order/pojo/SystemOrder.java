package com.pctx.bibapay.pay.order.pojo;

import java.util.Date;

public class SystemOrder {

	private Integer id;
	private String orderno;// 订单编号
	private String subject;// 订单标题，商品的标题/交易标题/订单标题/订单关键字等
	private String body;// 商品描述，该订单的备注、描述、明细等
	private Double money;// 订单金额
	private Date ordertime;// 订单创建日期
	private Date pay_time;// 支付时间
	private Double pay_money;// 实际支付金额
	private String way;// 下单渠道，PC(电脑网站),APP,JSSDK(微信公众号),JSAPI(微信小程序),H5(手机网站)
	private String ip;// 用户下单IP
	private String openid;// 付款用户的微信openid（同一公众号，同一个用户的openId相同）
	private String appid;// 发起支付的公众账号ID
	private String pay_way;// 支付方式, 支付宝alipay,银联unionpay,微信wxpay
	private String pay_trade_no;// 支付交易号
	private Integer order_type;// 订单类型，0:普创收款，1:普创付款
	private String business_model;// 业务模式,B2B、B2C、C2C
	private Integer buyer_id;// 买家用户id
	private Integer seller_id;// 卖家用户id

	private Double service_money;// 服务费
	private Double refund_money;// 订单退款金额
	private Integer status;// 订单状态，0:未付款，1:已付款，2:退款
	private String notify_url;
	private String return_url;
	private String remarks;// 订单备注
	
	private String created_by;
	private String last_updated_by;
	private Date created_date;
	private Date last_updated_date;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrderno() {
		return orderno;
	}

	public void setOrderno(String orderno) {
		this.orderno = orderno;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public Date getOrdertime() {
		return ordertime;
	}

	public void setOrdertime(Date ordertime) {
		this.ordertime = ordertime;
	}

	public Date getPay_time() {
		return pay_time;
	}

	public void setPay_time(Date pay_time) {
		this.pay_time = pay_time;
	}

	public Double getPay_money() {
		return pay_money;
	}

	public void setPay_money(Double pay_money) {
		this.pay_money = pay_money;
	}

	public String getWay() {
		return way;
	}

	public void setWay(String way) {
		this.way = way;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getPay_way() {
		return pay_way;
	}

	public void setPay_way(String pay_way) {
		this.pay_way = pay_way;
	}

	public String getPay_trade_no() {
		return pay_trade_no;
	}

	public void setPay_trade_no(String pay_trade_no) {
		this.pay_trade_no = pay_trade_no;
	}

	public Integer getOrder_type() {
		return order_type;
	}

	public void setOrder_type(Integer order_type) {
		this.order_type = order_type;
	}

	public String getBusiness_model() {
		return business_model;
	}

	public void setBusiness_model(String business_model) {
		this.business_model = business_model;
	}

	public Integer getBuyer_id() {
		return buyer_id;
	}

	public void setBuyer_id(Integer buyer_id) {
		this.buyer_id = buyer_id;
	}

	public Integer getSeller_id() {
		return seller_id;
	}

	public void setSeller_id(Integer seller_id) {
		this.seller_id = seller_id;
	}

	public Double getService_money() {
		return service_money;
	}

	public void setService_money(Double service_money) {
		this.service_money = service_money;
	}

	public Double getRefund_money() {
		return refund_money;
	}

	public void setRefund_money(Double refund_money) {
		this.refund_money = refund_money;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getReturn_url() {
		return return_url;
	}

	public void setReturn_url(String return_url) {
		this.return_url = return_url;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getLast_updated_by() {
		return last_updated_by;
	}

	public void setLast_updated_by(String last_updated_by) {
		this.last_updated_by = last_updated_by;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Date getLast_updated_date() {
		return last_updated_date;
	}

	public void setLast_updated_date(Date last_updated_date) {
		this.last_updated_date = last_updated_date;
	}

	
}