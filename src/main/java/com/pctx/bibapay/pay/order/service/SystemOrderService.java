package com.pctx.bibapay.pay.order.service;

import com.pctx.bibapay.pay.order.dao.SystemOrderMapper;
import com.pctx.bibapay.pay.order.pojo.SystemOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class SystemOrderService {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMddHHmmssSSS");
	@Autowired
	private SystemOrderMapper systemOrderDao;

	/**
	 * 创建普创收款系统订单
	 * 
	 * @param systemOrder
	 */
	public void createSystemOrder(SystemOrder systemOrder) {
		Date date = new Date();
		String orderno = generateOrderChargeNo();
		systemOrder.setOrderno(orderno);// 系统订单号
		systemOrder.setOrder_type(0);// 普创收款
		systemOrder.setStatus(0);
		systemOrder.setOrdertime(date);
		systemOrder.setCreated_date(date);
		systemOrderDao.saveSystemOrder(systemOrder);
	}

	/**
	 * 根据当前时间（精确到毫秒）生成订单号
	 * 
	 * @param prefix
	 *            订单号的前缀
	 * @return
	 */
	public String generateOrderChargeNo() {
		String orderNo = SDF.format(new Date());

		SystemOrder systemOrder = new SystemOrder();
		systemOrder.setOrderno(orderNo);
		SystemOrder oc = systemOrderDao.querySystemOrder(systemOrder);
		boolean b = oc != null ? true : false;
		while (b) {
			orderNo = SDF.format(new Date());
			systemOrder.setOrderno(orderNo);
			oc = systemOrderDao.querySystemOrder(systemOrder);
			b = oc != null ? true : false;
		}
		return orderNo;
	}

	public void saveSystemOrder(SystemOrder systemOrder) {
		systemOrderDao.saveSystemOrder(systemOrder);
	}

	public void updateSystemOrder(SystemOrder systemOrder) {
		systemOrderDao.updateSystemOrder(systemOrder);
	}

	public List<SystemOrder> querySystemOrderList(SystemOrder systemOrder) {
		return systemOrderDao.querySystemOrderList(systemOrder);
	}

	public SystemOrder querySystemOrder(SystemOrder systemOrder) {
		return systemOrderDao.querySystemOrder(systemOrder);
	}

}