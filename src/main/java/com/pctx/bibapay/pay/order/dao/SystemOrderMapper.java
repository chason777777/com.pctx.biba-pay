package com.pctx.bibapay.pay.order.dao;

import com.pctx.bibapay.pay.order.pojo.SystemOrder;

import java.util.List;

public interface SystemOrderMapper {

	public void saveSystemOrder(SystemOrder systemOrder);

	public SystemOrder querySystemOrder(SystemOrder systemOrder);

	public void updateSystemOrder(SystemOrder systemOrder);

	public List<SystemOrder> querySystemOrderList(SystemOrder systemOrder);

}